#!/bin/bash

echo "Lagu Hip Hop Populer"
grep "hip hop" playlist.csv | sort -t ',' -k15 -n -r | head -n 5
echo

echo "Lagu John Mayer"
grep "John Mayer" playlist.csv | sort -t ',' -k15 -n | head -n 5
echo

echo "Lagu Tahun 2004"
grep "2004" playlist.csv | sort -t ',' -k15 -n -r | head -n 10
echo

echo "Lagu Ciptaan Ibu Sri"
grep "Sri" playlist.csv
echo

