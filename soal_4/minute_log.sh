#!/bin/bash

timestamp=$(date +"%Y%M%d%H%m%s")
user="baeblue"
target_path="/home/$user/nomor-4/"

ram_metrics=$(free -m | awk 'NR==2 {print $2,$3,$4,$5,$6,$7}')
swap_metrics=$(free -m | awk 'NR==3 {print $2,$3,$4}')
path=$(du -sh "$target_path" | awk '{print $1}')

echo "$ram_metrics,$swap_metrics,$path" >> "metrics_${timestamp}.log"




#Cron_Job
#* * * * * /home/baeblue/nomor-4/minute_log.sh

