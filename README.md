# Soal 1
## Cara Pengerjaan

1. Mendownload playlist.csv dan menyimpannya ke directory yang dituju
```bash
wget -O [filename] --no-check-certificate -r 'https://drive.google.com/uc?export=download&id=1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp'
```
 <br>

2. Membuat file playlist_keren.sh
```bash
nano playlist_keren.sh
```
<br>

3. Membuat perintah untuk menampilkan  5 top teratas lagu dengan genre hip hop berdasarkan popularity.
```bash
!/bin/bash

echo "Lagu Hip Hop Populer"
grep "hip hop" playlist.csv | sort -t ',' -k15 -n -r | head -n 5 
echo

```
- **grep “hip hop” playlist.csv** untuk mencari text yang mengandung kata "hip hop" yang ada dalam playlist.csv.
- **sort -t ‘,’ -k15 -n -r** untuk mengurutkan sesuai dengan popularity (kolom ke-15) dan diurutkan secara terbalik (besar ke kecil).
- **head -n 5** untuk diambil urutan 5 teratas.

<br>

4. Membuat perintah untuk mencari 5 lagu yang paling rendah diantara lagu ciptaan John Mayer berdasarkan popularity.
```bash
echo "Lagu John Mayer"
grep "John Mayer" playlist.csv | sort -t ',' -k15 -n | head -n 5 
echo

```
- **grep “John Mayer” playlist.csv** untuk mencari text yang mengandung kata "John Mayer" yang ada dalam playlist.csv.
- **sort -t ‘,’ -k15 -n** untuk mengurutkan sesuai dengan popularity (kolom ke-15).
- **head -n 5** untuk diambil urutan 5 teratas.

<br>

5. Membuat perintah untuk mencari 10 lagu pada tahun 2004 dengan rank popularity tertinggi
```bash
echo "Lagu Tahun 2004"
grep "2004" playlist.csv | sort -t ',' -k15 -n -r | head -n 10 
echo

```
- **grep “2004” playlist.csv** untuk mencari text yang mengandung kata "2004" yang ada dalam playlist.csv.
- **sort -t ‘,’ -k15 -n -r** untuk mengurutkan sesuai dengan popularity (kolom ke-15) dan diurutkan secara terbalik (besar ke kecil).
- **head -n 5** untuk diambil urutan 5 teratas.

<br>

6. Membuat perintah untuk mencari lagu tersebut dengan kata kunci ibu Sri
```bash
echo "Lagu Ciptaan Ibu Sri"
grep "Sri" playlist.csv 
echo

```
- **grep “Sri” playlist.csv** untuk mencari text yang mengandung kata "Sri" yang ada dalam playlist.csv.

<br>

7. Menyimpan dan ubah permission file script agar dapat dieksekusi.
```bash
chmod +x playlist_keren.sh
```

<br>

8. Mengeksekusi program yang telah dibuat
```bash
bash playlist_keren.sh
```

<br>

9. Menampilkan output
![output 1 ](gambar/1-output.png)


## Kendala
Kesulitan untuk mendownload file  [playlist.csv](https://drive.google.com/file/d/1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp/view) ke dalam directory

## Revisi
input code:
```bash
!/bin/bash

echo "Lagu Hip Hop Populer"
grep "hip hop" playlist.csv | sort -t ',' -k15 -n -r | head -n 5 | awk -F ',' '{print $2,"," $4, "," $15}'
echo

echo "Lagu John Mayer"
grep "John Mayer" playlist.csv | sort -t ',' -k15 -n | head -n 5 | awk -F ',' '{print $2 "," $3 "," $15}'
echo

echo "Lagu Tahun 2004"
grep "2004" playlist.csv | sort -t ',' -k15 -n -r | head -n 10 | awk -F ',' '{print $2 "," $5 "," $15}'
echo

echo "Lagu Ciptaan Ibu Sri"
grep "Sri" playlist.csv | awk -F ',' '{print $2, "," $3, ","  $4, ","  $5, ","  $15}'
echo

```

<br>

Output:

![output rec](gambar/1-output-revisi.png)

# Soal 2
## Membuat Program Registrasi dan Login
## Script Registrasi User

1. Membuat file register.sh, lalu ketik perintah untuk membuka berkas file registrasi tersebut.

```bash
$ touch register.sh
$ nano register.sh
```

2. Buat perintah untuk meminta user memasukkan email, username, dan password.

``` bash
#!/bin/bash

read -p "Email: " email
read -p "Username: " username
read -s -p "Password: " password
```

3. Buat perintah untuk mengenkripskan password menggunakan metode base64.

``` bash
encrypted_password=$(echo -n "$password" | base64)
```

4. Buat perintah untuk memeriksa panjang password, harus lebih dari 8 karakter.

``` bash
if [ ${#password} -lt 8 ]; then
        echo "Password harus terdiri dari minimal 8 karakter."
        exit 1
fi
```

5. Buat perintah untuk memeriksa apakah password mengandung minimal 1 huruf kapital, 1 huruf kecil, 1 angka, dan 1 simbol unik.

``` bash
if ! [[ "$password" =~ [A-Z] && "$password" =~ [a-z] && "$password" =~ [0-9] && "$password" =~ [^a-zA-Z0-9] ]]; then
        echo "Password harus mengandung minimal 1 huruf kapital, 1 huruf kecil, 1 angka, dan 1 simbol unik."
        exit 1
fi
```

6. Buat perintah untuk memeriksa apakah password tidak sama dengan username.

``` bash
if [ "$password" == "$username" ]; then
        echo "Password tidak boleh sama dengan username."
        exit 1
fi
```

7. Buat perintah untuk memeriksa apakah email yang dimasukkan sudah terdaftar sebelumnya.

``` bash
if grep -q "^$email:" users/users.txt; then
```

8. Jika email yang dimasukkan belum terdaftar, maka otomatis akan disimpan data registrasi (email, username, dan password terenkripsi) ke dalam file users/users.txt.

``` bash
else
        echo "$email:$username:$encrypted_password" >> users/users.txt
fi
```

berikut ini adalah output data user yang tersimpan pada file users.txt:

![data users](gambar/datausers.png)

## Respon Registrasi

1. Memberikan respon apakah registrasi berhasil atau gagal.
#### Registrasi berhasil:

``` bash
echo "Berhasil terdaftar"
echo "[REGISTER SUCCESS] user $username registered successfully"
```

Full code:
``` bash
else
        echo "$email:$username:$encrypted_password" >> users/users.txt
        echo "Berhasil terdaftar"
        echo "[REGISTER SUCCESS] user $username registered successfully"

```

#### Registrasi gagal:
- Password kurang dari 8 karakter.

``` bash
echo "[REGISTER FAILED] user $username registration failed (invalid password)"
```

Full code:

``` bash
if [ ${#password} -lt 8 ]; then
        echo "Password harus terdiri dari minimal 8 karakter."
        echo "[REGISTER FAILED] user $username registration failed (invalid password)"
        exit 1
fi
```

- Password minimal 1 huruf kapital, 1 huruf kecil, 1 angka, dan 1 simbol unik.

``` bash
echo "[REGISTER FAILED] user $username registration failed (invalid password)"
```

Full code:

``` bash
if ! [[ "$password" =~ [A-Z] && "$password" =~ [a-z] && "$password" =~ [0-9] && "$password" =~ [^a-zA-Z0-9] ]]; then
        echo "Password harus mengandung minimal 1 huruf kapital, 1 huruf kecil, 1 angka, dan 1 simbol unik."
        echo "[REGISTER FAILED] user $username registration failed (invalid password)"
        exit 1
fi
```

- Password sama dengan username.

``` bash
echo "[REGISTER FAILED] user $username registration failed (password same as username)"
```

Full code:

``` bash
if [ "$password" == "$username" ]; then
        echo "Password tidak boleh sama dengan username."
        echo "[REGISTER FAILED] user $username registration failed (password same as username)"
        exit 1
fi
```

- Email sudah terdaftar.

``` bash
echo "[REGISTER FAILED] user $username registration failed (email is already in use)"
```

Full code:

``` bash
if grep -q "^$email:" users/users.txt; then
        echo "Email tersebut sudah terdaftar, gunakan email lain!"
        echo "[REGISTER FAILED] user $username registration failed (email is already in use)"
```

## Script Login User

1. Membuat file login.sh, lalu ketik command untuk membuka berkas file registrasi tersebut.

``` sh
$ touch login.sh  
$ nano login.sh
```

2. Buat perintah untuk meminta user memasukkan email dan password.

``` bash
#!/bin/bash

read -p "Masukkan email: " email
read -s -p "Masukkan password: " password
```

3. Buat perintah untuk memeriksa apakah email terdaftar dalam file users/users.txt.

``` bash
if grep -q "^$email:" users/users.txt; then
```

4. Jika email terdaftar, lalu buat perintah  untuk mendapatkan dan mendekripsi password yang disimpan dalam berkas users/users.txt berdasarkan email yang diberikan.

``` bash
stored_password=$(grep "^$email:" users/users.txt | cut -d':' -f3 | base64 -d)
```

5. Buat perintah untuk memeriksa apakah password yang dimasukkan oleh pengguna sama dengan password yang tersimpan setelah didekripsi.

``` bash
 if [ "$password" == "$stored_password" ]; then
```

6. Buat perintah untuk mengambil username dari berkas users/users.txt berdasarkan email yang diberikan. Ini dibutuhkan saat menampilkan respon login menggunakan variabel username.

``` bash
username=$(grep "^$email:" users/users.txt | cut -d':' -f2)
```

## Respon Login

1. Memberikan respon login berhasil.

``` bash
echo "Login sukses! Welcome, $username."
```

Full code:

``` bash
 if [ "$password" == "$stored_password" ]; then
                echo "Login sukses! Welcome, $username."
        else
```

2. Memberikan respon login gagal:
    * Email tidak terdaftar.

    ``` bash
    echo "Login gagal! email $email tidak terdaftar, harap registrasi dahulu."
    ```

    Full code:

    ``` bash
    else
    echo "Login gagal! email $email tidak terdaftar, harap registrasi dahulu."
    fi
    ```

    * Password salah.

    ``` bash
    echo "Login gagal! Password yang anda masukkan salah."
    ```

    Full code:
    ``` bash
    else
    echo "Login gagal! Password yang anda masukkan salah."
    fi
    ```

## Menyimpan Data Registrasi dan Log Aktivitas

Aktivitas registrasi dan login dicatat dalam file users/auth.log. **Format** catatan adalah [dd/mm/yy HH:MM:SS] [TIPE AKTIVITAS] pengguna [username] [keterangan].

1. Aktivitas Registrasi
    * Registrasi berhasil
    ``` bash
    echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER SUCCESS] user $username registered successfully" >> users/auth.log
    ```

    * Registrasi gagal

        i. Password kurang dari 8 karakter.
        ``` bash
        echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER FAILED] user $username registration failed (invalid password)" >> users/auth.log
        ```

        ii. Password minimal 1 huruf kapital, 1 huruf kecil, 1 angka, dan 1 simbol unik.
        ``` bash
        echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER FAILED] user $username registration failed (invalid password)" >> users/auth.log
        ```

        iii. Password sama dengan username.
        ``` bash
        echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER FAILED] user $username registration failed (password same as username)" >> users/auth.log
        ```

        iv. Email sudah terdaftar.
        ``` bash
        echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER FAILED] user $username registration failed (email is already in use)" >> users/auth.log
        ```

2. Aktivitas Registrasi
    * Login berhasil
    ``` bash
    echo "[$(date +"%d/%m/%y %H:%M:%S")] [LOGIN SUCCESS] user $username logged in successfully" >> users/auth.log
    ```

    * Login gagal

        i. Email tidak terdaftar.
        ``` bash
        echo "[$(date +"%d/%m/%y %H:%M:%S")] [LOGIN FAILED] ERROR Failed login attempt on user with email $email" >> users/auth.log
        ```

        ii. Password salah.
        ``` bash
        echo "[$(date +"%d/%m/%y %H:%M:%S")] [LOGIN FAILED] ERROR Failed login attempt on user with email $email" >> users/auth.log
        ```

        berikut ini adalah output aktivitas log yang tersimpan pada file auth.log:

![aktivitas log](gambar/log-aktivitas.png)

## Kode Lengkap Script Registrasi

``` bash
#!/bin/bash

read -p "Email: " email
read -p "Username: " username
read -s -p "Password: " password

if [ ${#password} -lt 8 ]; then
        echo "Password harus terdiri dari minimal 8 karakter."
        echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER FAILED] user $username registration failed (invalid password)" >> users/auth.log
        exit 1
fi

if ! [[ "$password" =~ [A-Z] && "$password" =~ [a-z] && "$password" =~ [0-9] && "$password" =~ [^a-zA-Z0-9] ]]; then
        echo "Password harus mengandung minimal 1 huruf kapital, 1 huruf kecil, 1 angka, dan 1 simbol unik."
        echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER FAILED] user $username registration failed (invalid password)" >> users/auth.log
        exit 1
fi

if [ "$password" == "$username" ]; then
        echo "Password tidak boleh sama dengan username."
        echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER FAILED] user $username registration failed (password same as username)" >> users/auth.log
        exit 1
fi

encrypted_password=$(echo -n "$password" | base64)

if grep -q "^$email:" users/users.txt; then
        echo "Email tersebut sudah terdaftar, gunakan email lain!"
        echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER FAILED] user $username registration failed (email is already in use)" >> users/auth.log
else
        echo "$email:$username:$encrypted_password" >> users/users.txt
        echo "Berhasil terdaftar"
        echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER SUCCESS] user $username registered successfully" >> users/auth.log
fi
```

Output:

![output registrasi](gambar/output-registrasi.png)

## Kode Lengkap Script Login

``` bash
    #!/bin/bash

read -p "Masukkan email: " email
read -s -p "Masukkan password: " password

username=$(grep "^$email:" users/users.txt | cut -d':' -f2)

if grep -q "^$email:" users/users.txt; then
    stored_password=$(grep "^$email:" users/users.txt | cut -d':' -f3 | base64 -d)
        if [ "$password" == "$stored_password" ]; then
                echo "Login sukses! Welcome, $username."
                echo "[$(date +"%d/%m/%y %H:%M:%S")] [LOGIN SUCCESS] user $username logged in successfully" >> users/auth.log
        else
                echo "Login gagal! Password yang anda masukkan salah."
                echo "[$(date +"%d/%m/%y %H:%M:%S")] [LOGIN FAILED] ERROR Failed login attempt on user with email $email" >> users/auth.log
        fi
else
        echo "Login gagal! email $email tidak terdaftar, harap registrasi dahulu."
        echo "[$(date +"%d/%m/%y %H:%M:%S")] [LOGIN FAILED] ERROR Failed login attempt on user with email $email" >> users/auth.log
fi
```

Output:

![output login](gambar/output-login.png)

# Soal 3
## Cara Pengerjaan

1. Membuat file genshin.sh
```bash
touch genshin.sh
```
 <br>

2. Mengubah akses file genshin.sh agar dapat dijalankan
```bash
chmod +x genshin.sh
```
<br>

3. Mengedit file genshin.sh
```bash
nano genshin.sh
```
<br>

4. Buka link [Link file genshin.zip](https://drive.google.com/file/d/1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2/view), lalu dapatkan ID file yang ingin didownload
<br>

5. Download file dengan script bash
```bash
wget -O "$genshin_zip" --no-check-certificate "https://drive.google.com/uc?id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2"
```
<br>

6. Gunakan variabel untuk mempermudah
```bash
genshin_zip="genshin.zip"
genshin_character_zip="genshin_character.zip"
list_character="list_character.csv"
```
<br>

7. Cek apakah file genshin.zip sudah didownload
```bash
if [ ! -f "$genshin_zip" ]; then
	echo "Error: File '$genshin_zip' tidak ada."
	exit 1
fi
```
<br>

8. Unzip genshin.zip
```bash
unzip "$genshin_zip"
```
<br>

9. Cek apakah unzip berhasil
```bash
if [ $? -eq 0 ]; then
	echo "Unzip '$genshin_zip' berhasil."
else
	echo "Error: Gagal unzip '$genshin_zip'."
	exit 1
fi
```
<br>

10. Cek apakah file genshin_character.zip ada
```bash
if [ ! -f "$genshin_character_zip" ]; then
	echo "Error: File '$genshin_character_zip' tidak ada."
	exit 1
fi
```
<br>

11. Unzip file genshin_character.zip
```bash
unzip "$genshin_character_zip"
```
<br>

12. Cek apakah unzip berhasil
```bash
if [ $? -eq 0 ]; then
	echo "Unzip '$genshin_character_zip' berhasil."
else
	echo "Error: Gagal unzip '$genshin_character_zip'."
	exit 1
fi
```
<br>

13. Buat folder genshin_character
```bash
mkdir -p genshin_character
```
<br>

14. Buat perulangan untuk decode setiap nama file.jpg
```bash
for encoded_filename in *.jpg; do
	decoded_filename=$(echo "$encoded_filename" | base64 -d 2>/dev/null)
done
```
<br>

15. Tambahkan dalam perulangan dengan grep untuk mencocokkan nama file.jpg dengan list dalam file list_character.csv
```bash
cocoklogi=$(grep -F "$decoded_filename" "$list_character")
```
<br>

16. Tambahkan dalam perulangan untuk mengecek apakah nama file.jpg cocok dengan list dalam list_character.csv
```
if [ -z "$cocoklogi" ]; then
    	echo "Error: Tidak ada baris yang cocok untuk '$decoded_filename' dalam '$list_character'."
    	exit 1
fi
```
<br>

17. Tambahkan dalam perulangan dengan deklarasi variabel untuk setiap kolom dalam list_character.csv
```bash
nama=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $1}')
region=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $2}')
elemen=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $3}')
senjata=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $4}')
```
<br>

18. Tambahkan dalam perulangan untuk variabel yang akan menjadi nama file.jpg yang baru
```bash
namabaru="${nama}-${region}-${elemen}-${senjata}"
```
<br>

19. Tambahkan dalam perulangan untuk mengubah nama file.jpg menjadi format nama yang baru
```bash
mv "$encoded_filename" "genshin_character/${namabaru}.jpg"
echo "Merename: $encoded_filename -> genshin_character/${namabaru}.jpg"
```
<br>

20. Menghitung setiap pengguna dari senjata dalam list_character.csv
```bash
jumlah_pengguna=$(cut -d ',' -f 4 "$list_character" | tail -n +2 | tr -d '\r' | sort | uniq -c)
```
<br>

21. Mengoutput jumlah pengguna dari setiap senjata sesuai format
```bash
while read -r line; do
   	jumlah=$(echo "$line" | awk '{print $1}')
        nama_senjata=$(echo "$line" | awk '{print $2}')
   	echo "Jumlah pengguna ($nama_senjata) : $jumlah"
done <<< "$jumlah_pengguna"
```
<br>

22. Menghapus file genshin_character.zip, list_character.csv, dan genshin.zip
```bash
rm genshin_character.zip list_character.csv genshin.zip
```
<br>

23. Jalankan program
```bash
./genshin.sh
```
<br>

- **Hasil output program** <br>
![output genshin ](gambar/output_genshin.png)

- **Script full bagian genshin** <br>
```bash
#!/bin/bash

genshin_zip="genshin.zip"
genshin_character_zip="genshin_character.zip"
list_character="list_character.csv"

wget -O "$genshin_zip" --no-check-certificate "https://drive.google.com/uc?id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2"

if [ ! -f "$genshin_zip" ]; then
    echo "Error: File '$genshin_zip' tidak ada."
    exit 1
fi

unzip "$genshin_zip"

if [ $? -eq 0 ]; then
    echo "Unzip '$genshin_zip' berhasil."
else
    echo "Error: Gagal unzip '$genshin_zip'."
    exit 1
fi

if [ ! -f "$genshin_character_zip" ]; then
    echo "Error: File '$genshin_character_zip' tidak ada."
    exit 1
fi

unzip "$genshin_character_zip"

if [ $? -eq 0 ]; then
    echo "Unzip '$genshin_character_zip' berhasil."
else
    echo "Error: Gagal unzip '$genshin_character_zip'."
    exit 1
fi

mkdir -p genshin_character

for encoded_filename in *.jpg; do
    decoded_filename=$(echo "$encoded_filename" | base64 -d 2>/dev/null)
    cocoklogi=$(grep -F "$decoded_filename" "$list_character")
    
    if [ -z "$cocoklogi" ]; then
        echo "Error: Tidak ada baris yang cocok untuk '$decoded_filename' dalam '$list_character'."
        exit 1
    fi

    nama=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $1}')
    region=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $2}')
    elemen=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $3}')
    senjata=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $4}')
    
    namabaru="${nama}-${region}-${elemen}-${senjata}"

    mkdir -p "genshin_character/$region"

    mv "$encoded_filename" "genshin_character/$region/${namabaru}.jpg"
    echo "Merename: $encoded_filename -> genshin_character/$region/${namabaru}.jpg"
done

jumlah_pengguna=$(cut -d ',' -f 4 "$list_character" | tail -n +2 | tr -d '\r' | sort | uniq -c)

while read -r line; do
    jumlah=$(echo "$line" | awk '{print $1}')
    nama_senjata=$(echo "$line" | awk '{print $2}')
    echo "Jumlah pengguna ($nama_senjata) : $jumlah"
done <<< "$jumlah_pengguna"
rm genshin_character.zip list_character.csv genshin.zip
```
<br>

## Kendala
error unzip file genshin.zip<br>
error rename file .jpg dari list_character.csv<br>
error dalam input passphrase steghide<br>
error dalam encode base64 setelah ekstrak steghide<br>
program tidak cocok untuk multithreading karena saling berebut sehingga hasil dapat bercampur<br>

## Revisi

Untuk bagian C dan d
1. Membuat file find_me.sh
```bash
touch find_me.sh
```
<br>

2. Mengubah akses file agar bisa dijalankan
```bash
chmod +x find_me.sh
```
<br>

3. Mengedit file find_me.sh
```bash
nano find_me.sh
```
<br>

4. Deklarasi variabel untuk memudahkan
```bash
#!/bin/bash

log_file="image.log"
exit_flag=false
```
<br>

5. Perulangan untuk mengecek semua file .jpg dan melakukan ekstrak
```bash
while [ "$exit_flag" = false ]; do
    find genshin_character -type f -name "*.jpg" | while read jpg_file; do
        steghide extract -sf "$jpg_file" -p "" > /dev/null 2>&1
        txt_files=$(find . -type f -name "*.txt")
        sleep 1
        found_url=false
done
```
<br>

6. Menggunakan perulangan untuk decode isi setiap file .txt
```bash
while read -r txt_file; do
        image_path="$jpg_file"
        decoded_content=$(base64 -d -i "$txt_file" 2>/dev/null)
done<<< "$txt_files"
```
<br>

7. Mengecek apakah file .txt sudah didecode dan diganti isinya
```bash
if [ -n "$decoded_content" ]; then
        echo "File $txt_file telah didekode dan isinya telah diganti."
        echo "$decoded_content" > "$txt_file"
fi
```
<br>

8. Mencari URL dan mengaktifkan flag url sudah ditemukan
```bash
if grep -qE 'https?://[^[:space:]]+' <<< "$decoded_content"; then
        url="$decoded_content"
        echo "URL yang dicari ditemukan: $url"
        wget "$url"
        found_url=true
fi
```
<br>

9. Menghapus setiap file .txt yang tidak berisi URL
```bash
else
        echo "File $txt_file tidak berisi URL. Menghapusnya."
        rm "$txt_file"
```
<br>

10. Output image.log dengan hasil penemuan URL dan menghentikan program jika URL ditemukan
```bash
if [ "$found_url" = true ]; then
        echo "[$(date '+%d/%m/%y %H:%M:%S')] [FOUND] [$image_path]" >> "$log_file"
        exit_flag=true  
        break           
else
        echo "[$(date '+%d/%m/%y %H:%M:%S')] [NOT FOUND] [$image_path]" >> "$log_file"
fi
```
- **Hasil output program** <br>
![output find_me ](gambar/output_find_me.png)

- **Script full bagian genshin** <br>
```bash
#!/bin/bash

log_file="image.log"
exit_flag=false

while [ "$exit_flag" = false ]; do
    find genshin_character -type f -name "*.jpg" | while read jpg_file; do
        steghide extract -sf "$jpg_file" -p "" > /dev/null 2>&1
        txt_files=$(find . -type f -name "*.txt")
        sleep 1
        found_url=false

        while read -r txt_file; do
            image_path="$jpg_file"

            decoded_content=$(base64 -d -i "$txt_file" 2>/dev/null)

            if [ -n "$decoded_content" ]; then
                echo "File $txt_file telah didekode dan isinya telah diganti."
                echo "$decoded_content" > "$txt_file"

                if grep -qE 'https?://[^[:space:]]+' <<< "$decoded_content"; then
                    url="$decoded_content"
                    echo "URL yang dicari ditemukan: $url"
                    wget "$url"
                    found_url=true
                fi
            else
                echo "File $txt_file tidak berisi URL. Menghapusnya."
                rm "$txt_file"
            fi

            if [ "$found_url" = true ]; then
                echo "[$(date '+%d/%m/%y %H:%M:%S')] [FOUND] [$image_path]" >> "$log_file"
                exit_flag=true  
                break           
            else
                echo "[$(date '+%d/%m/%y %H:%M:%S')] [NOT FOUND] [$image_path]" >> "$log_file"
            fi
        done <<< "$txt_files"
    done
done
```
<br>

## Hasil Akhir
- **Directory** <br>
![hasil directory ](gambar/hasil_directory.png)
<br>

- **Tree folder genshin_character** <br>
![hasil tree ](gambar/hasil_tree.png)
<br>

- **Isi File Lisa.txt**
![hasil Lisa.txt ](gambar/hasil_Lisa.png)
<br>

- **Isi File image.log**
![hasil image.log ](gambar/hasil_image.png)
<br>

# Soal 4
## Cara Pengerjaan

1. Mengecek RAM dengan command free -m<br>
![gambar 4a ](gambar/4a.png)
<br>

2. Mengecek size directory dengan command  du -sh<br>
![gambar 4b ](gambar/4b.png)
<br>

3. Membuat file minute_log.sh
```bash
nano minute_log.sh
```
<br>

4. Membuat program yang dapat menampilkan metric_timestamp.log yang dapat dijalankan setiap menit, serta semua file lognya hanya bisa dibaca oleh pemilik file
```bash
!/bin/bash

timestamp=$(date +"%Y%m%d%H%M%S")
user="baeblue"
target_path="/home/$user/nomor-4/"

ram_metrics=$(free -m | awk 'NR==2 {print $2,$3,$4,$5,$6,$7}')
swap_metrics=$(free -m | awk 'NR==3 {print $2,$3,$4}')
path=$(du -sh "$target_path" | awk '{print $1}')

echo "$ram_metrics,$swap_metrics,$path" >> "metrics_${timestamp}.log"

chmod 600 "metrics_${timestamp}.log"


#Cron_Job
#* * * * * /home/baeblue/nomor-4/minute_log.sh
```
<br>

Penjelasan code:
- untuk menyimpan format tahun, bulan, hari, jam, menit dan detik serta menyimpannya dalam variable 'timestamp'.
```bash
timestamp=$(date +"%Y%m%d%H%M%S")
```
<br>

- mendefinisikan user sebagai 'baeblue' dan mendefinisikan directory dari target_path.
```bash
user="baeblue"
target_path="/home/$user/nomor-4/"
```
<br>

- mengambil informasi RAM dengan command 'free -m' lalu mencetak (awk) hasil dari baris kedua (NR==2) dengan kolom 2 - kolom 7.
```bash
ram_metrics=$(free -m | awk 'NR==2 {print $2,$3,$4,$5,$6,$7}')
```
<br>

- sama seperti sebelumnya, tapi mengambil informasi swap dari baris ketiga (NR==3) dan kolom 2 - 4.
```bash
swap_metrics=$(free -m | awk 'NR==3 {print $2,$3,$4}')
```
<br>

- untuk menghitung ukuran direktori yang ditentukan ($target_path) kemudian menggunakan 'awk' untuk mencetak ukurannya.
```bash
path=$(du -sh "$target_path" | awk '{print $1}')
```
<br>

- untuk mencetak metrik yang telah dikumpulkan ke dalam file log dengan nama "metrics_.log".
```bash
echo "$ram_metrics,$swap_metrics,$path" >> "metrics_${timestamp}.log"
```
<br>

- membuat file hanya bisa dibaca oleh user pemilik
```bash
chmod 600 "metrics_${timestamp}.log"
```
<br>

- agar file dapat berjalan otomatis setiap menit
```bash
#Cron_Job
#* * * * * /home/baeblue/nomor-4/minute_log.sh
```
<br>

5. Menyimpan dan ubah permission file script agar dapat dieksekusi.
```bash
chmod +x minute_log.sh 
```
<br>

6. Mengeksekusi file minute_log.sh
```bash
bash minute_log.sh
```
<br>

7. Menampilkan hasil eksekusi<br>
![gambar 4c ](gambar/4c.png)
<br>

8. Menampilan hasil metricnya<br>
![gambar 4d ](gambar/4d.png)
<br>
