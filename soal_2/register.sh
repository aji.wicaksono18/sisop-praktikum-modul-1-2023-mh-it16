#!/bin/bash

read -p "Email: " email
read -p "Username: " username
read -s -p "Password: " password

if [ ${#password} -lt 8 ]; then
	echo "Password harus terdiri dari minimal 8 karakter."
	echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER FAILED] user $username registration failed (invalid password)" >> users/auth.log
    	exit 1
fi

if ! [[ "$password" =~ [A-Z] && "$password" =~ [a-z] && "$password" =~ [0-9] && "$password" =~ [^a-zA-Z0-9] ]]; then
    	echo "Password harus mengandung minimal 1 huruf kapital, 1 huruf kecil, 1 angka, dan 1 simbol unik."
	echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER FAILED] user $username registration failed (invalid password)" >> users/auth.log
    	exit 1
fi

if [ "$password" == "$username" ]; then
	echo "Password tidak boleh sama dengan username."
	echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER FAILED] user $username registration failed (password same as username)" >> users/auth.log
    	exit 1
fi

encrypted_password=$(echo -n "$password" | base64)

if grep -q "^$email:" users/users.txt; then
	echo "Email tersebut sudah terdaftar, gunakan email lain!"
	echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER FAILED] user $username registration failed (email is already in use)" >> users/auth.log
else
	echo "$email:$username:$encrypted_password" >> users/users.txt
	echo "Berhasil terdaftar"
	echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER SUCCESS] user $username registered successfully" >> users/auth.log
fi

