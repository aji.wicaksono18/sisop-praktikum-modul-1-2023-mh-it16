#!/bin/bash

read -p "Masukkan email: " email
read -s -p "Masukkan password: " password

username=$(grep "^$email:" users/users.txt | cut -d':' -f2)

if grep -q "^$email:" users/users.txt; then
    stored_password=$(grep "^$email:" users/users.txt | cut -d':' -f3 | base64 -d)
	if [ "$password" == "$stored_password" ]; then
		echo "Login sukses! Welcome, $username."
		echo "[$(date +"%d/%m/%y %H:%M:%S")] [LOGIN SUCCESS] user $username logged in successfully" >> users/auth.log
	else
        	echo "Login gagal! Password yang anda masukkan salah."
		echo "[$(date +"%d/%m/%y %H:%M:%S")] [LOGIN FAILED] ERROR Failed login attempt on user with email $email" >> users/auth.log
	fi
else
	echo "Login gagal! email $email tidak terdaftar, harap registrasi dahulu."
	echo "[$(date +"%d/%m/%y %H:%M:%S")] [LOGIN FAILED] ERROR Failed login attempt on user with email $email" >> users/auth.log
fi

