#!/bin/bash

genshin_zip="genshin.zip"
genshin_character_zip="genshin_character.zip"
list_character="list_character.csv"

wget -O "$genshin_zip" --no-check-certificate "https://drive.google.com/uc?id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2"

if [ ! -f "$genshin_zip" ]; then
    echo "Error: File '$genshin_zip' tidak ada."
    exit 1
fi

unzip "$genshin_zip"

if [ $? -eq 0 ]; then
    echo "Unzip '$genshin_zip' berhasil."
else
    echo "Error: Gagal unzip '$genshin_zip'."
    exit 1
fi

if [ ! -f "$genshin_character_zip" ]; then
    echo "Error: File '$genshin_character_zip' tidak ada."
    exit 1
fi

unzip "$genshin_character_zip"

if [ $? -eq 0 ]; then
    echo "Unzip '$genshin_character_zip' berhasil."
else
    echo "Error: Gagal unzip '$genshin_character_zip'."
    exit 1
fi

mkdir -p genshin_character

for encoded_filename in *.jpg; do
    decoded_filename=$(echo "$encoded_filename" | base64 -d 2>/dev/null)
    cocoklogi=$(grep -F "$decoded_filename" "$list_character")
    
    if [ -z "$cocoklogi" ]; then
        echo "Error: Tidak ada baris yang cocok untuk '$decoded_filename' dalam '$list_character'."
        exit 1
    fi

    nama=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $1}')
    region=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $2}')
    elemen=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $3}')
    senjata=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $4}')
    
    namabaru="${nama}-${region}-${elemen}-${senjata}"

    mkdir -p "genshin_character/$region"

    mv "$encoded_filename" "genshin_character/$region/${namabaru}.jpg"
    echo "Merename: $encoded_filename -> genshin_character/$region/${namabaru}.jpg"
done

jumlah_pengguna=$(cut -d ',' -f 4 "$list_character" | tail -n +2 | tr -d '\r' | sort | uniq -c)

while read -r line; do
    jumlah=$(echo "$line" | awk '{print $1}')
    nama_senjata=$(echo "$line" | awk '{print $2}')
    echo "Jumlah pengguna ($nama_senjata) : $jumlah"
done <<< "$jumlah_pengguna"
