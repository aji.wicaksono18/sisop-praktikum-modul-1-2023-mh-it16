#!/bin/bash

folder_path="genshin_character"

log_file="image.log"

log() {
  local log_message="$1"
  local log_type="$2"
  local image_path="$3"
  local current_time=$(date "+[%d/%m/%y %H:%M:%S]")

  echo "$current_time [$log_type] [$image_path] $log_message" >> "$log_file"
}
process_image() {
  local image_path="$1"
  local steghide_output=$(steghide extract -sf "$image_path" -p "" 2>&1)

  if [[ $steghide_output == *"is not a steganography file."* ]]; then
    log "Not a steganography file" "NOT FOUND" "$image_path"
    local txt_file="${image_path%.jpg}.txt"
    [[ -e "$txt_file" ]] && rm "$txt_file"
  elif [[ $steghide_output == *"wrote extracted data to"* ]]; then
    log "Found steganography file" "FOUND" "$image_path"
    local txt_file="${image_path%.jpg}.txt"

    if [[ -e "$txt_file" ]]; then
      local base64_url=$(cat "$txt_file" | base64 -d)

      if [[ $base64_url == http* ]]; then
        log "Found valid URL: $base64_url" "URL" "$image_path"
        wget "$base64_url" && exit 0
      else
        log "Invalid URL: $base64_url" "INVALID URL" "$image_path"
      fi
    fi
  fi
}

while true; do
  for image in "$folder_path"*.jpg; do
    if [[ -f "$image" ]]; then
      process_image "$image"
    fi
  done
  sleep 1
done
